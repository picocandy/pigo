package pigo

import (
	"net/http"
)

// Serve custom 404 error page. It will just display "not found" message along with 404 status code.
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "not found", http.StatusNotFound)
}
