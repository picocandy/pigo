package pigo

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHttpNotFound(t *testing.T) {
	mux := http.NewServeMux()
	ts := httptest.NewServer(mux)
	defer ts.Close()

	res, err := http.Get(ts.URL + "/not-exist")
	if err != nil {
		t.Error(err)
	}

	if res.StatusCode != http.StatusNotFound {
		t.Errorf("Status Code %q => %d != %d", ts.URL, res.StatusCode, http.StatusOK)
	}
}
