package pigo

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

type Command struct {
	Name        string
	Environment *string
	Address     *string
	Port        *int
	Help        *bool
}

var usage = `
Server Options:
    -env=ENVIRONMENT	Set the execution environment (default: development)
    -address=ADDRESS	Bind to HOST address (default: 0.0.0.0)
    -port=PORT		Use PORT (default: 5020)
    -h/-help		Display help message
`

func NewCommand(name string) *Command {
	c := new(Command)
	c.Name = name
	c.Environment = flag.String("env", "development", "Set the execution environment (default: development)")
	c.Address = flag.String("address", "0.0.0.0", "Bind to HOST address (default: 0.0.0.0)")
	c.Port = flag.Int("port", 5020, "Use PORT (default: 5020)")
	c.Help = flag.Bool("help", false, "Display help message")

	// Overrides default flag.Usage
	flag.Usage = func() {
		helpUsage := fmt.Sprintf("Usage: %s [options]\n%s", c.Name, usage)
		fmt.Fprint(os.Stderr, strings.TrimLeft(helpUsage, "\n"))
	}

	return c
}

func (c *Command) Parse() {
	flag.Parse()

	if *c.Help {
		flag.Usage()
		os.Exit(2)
	}

	if os.Getenv("ENV") == "" {
		os.Setenv("ENV", *c.Environment)
	}
}

func (c *Command) ListenTo() string {
	return fmt.Sprintf("%s:%d", *c.Address, *c.Port)
}
