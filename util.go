package pigo

import (
	"os"
	"path/filepath"
)

func RelPath(name ...string) (string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	pth := append([]string{pwd}, name...)
	return filepath.Join(pth...), nil
}
