package pigo

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var ServerName = "Picocandy"

func NewRouter() *mux.Router {
	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(NotFoundHandler)

	return r
}

// Prepare HTTP handler. It will log the request and set the server name.
func MakeHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Set the server name
		w.Header().Set("Server", ServerName)

		// Logs current request
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)

		// Serve HTTP
		handler.ServeHTTP(w, r)
	})
}

func APIHandler(handler http.Handler) http.Handler {
	// Do not respond request for /favicon.ico
	faviconHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/favicon.ico" {
			NotFoundHandler(w, r)
			return
		}

		handler.ServeHTTP(w, r)
	})

	return MakeHandler(faviconHandler)
}
