# Pigo

Common library for Golang based application.

## Usage

To use `pigo` library you can do:

```
$ cd $GOPATH/src/bitbucket.org/picocandy
$ git clone git@bitbucket.org:picocandy/pigo.git
```

Then on Golang project, you can import:

```
package main

import (
    "bitbucket.org/picocandy/pigo"
)

func main() {
    // use pigo here
}
```
